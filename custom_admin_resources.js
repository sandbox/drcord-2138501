/* Feynman Group 2013 - Cord Slatton-Valle
	Custom js file for loading in Drupal admin
*/ 

window.Custom = window.Custom || {};


Custom.Admin = {
	init: function(){
		    Custom.Admin.doStuff();
	},
	doStuff : function(){
		//if I were a real function I woudl do something!
	}
}

//do stuff
jQuery(function(){
	Custom.Admin.init();
});